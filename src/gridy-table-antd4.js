

import { GridyTableImpl } from "../../gridy-grid/src/table/impl/gridy-table-impl.js";



export class GridyTableAntd4 extends GridyTableImpl {
	
	get selectedCssClass() {
		return 'ant-table-row-selected';
	}

}